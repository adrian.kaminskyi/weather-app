import React, { useState,useEffect } from 'react'
import { useDispatch, useStore } from 'react-redux';
import { NavLink } from 'react-router-dom';
import removeItem from '../store/actionCreators/removeItem';
import updateItem from '../store/actionCreators/updateItem';

export default function Card() {
    
    const [cities, setCities] = useState([]);
    const [toggle, setToggle] = useState(false);
    const API_KEY = '84d4806cf446eaaca48f246a981554ac';
    const dispatch = useDispatch();
    const store = useStore();
    store.subscribe(() => {
        setToggle(prev => !prev);
    });

    useEffect(() => {
        if (localStorage.getItem('cities')) {
            setCities(prev => [...prev, ...JSON.parse(localStorage.getItem('cities'))]);
        }
        return () => {
            setCities([]);
        }
    }, [toggle]);

    async function handleUpdate (e, cityId) {
        try {
            e.preventDefault();
            const data = await fetch(`http://api.openweathermap.org/data/2.5/weather?id=${cityId}&appid=${API_KEY}`);
            const city = await data.json();
            const cityArr = JSON.parse(localStorage.getItem('cities'));
            const oldCityIndex = cityArr.findIndex(city => city.id === cityId);
            cityArr.splice(oldCityIndex, 1, city);
            dispatch(updateItem(cityId));
        } catch (err) { }
    }

    function handleDelete (e, cityId) {
        e.preventDefault();
        const cityArr = JSON.parse(localStorage.getItem('cities'))
        const updatedCityArr = cityArr.filter(element => element.id !== cityId)
        localStorage.setItem('cities',JSON.stringify(updatedCityArr))
        dispatch(removeItem(cityId));
    }

    return (
        <ul className="container cards-wrap">
            {cities && cities.map(city => {
                return (
                <li className="card" key={city.id}>
                    <div className="card-body">
                        <NavLink to={{pathname:`/about/${city.id}`, city}} className="h5 card-title">{city.name}</NavLink>
                        <h6 className="card-subtitle mb-2 text-muted">{city.weather[0].main}</h6>
                        <p className="card-text">{city.weather && city.weather[0].description}</p>
                        <button onClick={e => handleUpdate(e, city.id)} className="card-link">Update</button>
                        <button onClick={e => handleDelete(e, city.id)} className="card-link">Delete</button>
                    </div>
                </li>
            )})}
        </ul>
    )
}
