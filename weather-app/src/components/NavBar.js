import React from 'react'
import { NavLink } from 'react-router-dom'

export default function NavBar() {
    return (
        <div>
            <nav className="navbar navbar-light bg-light">
                <NavLink className="navbar-brand" to={'/'}>Navbar</NavLink>
            </nav>
        </div>
    )
}
