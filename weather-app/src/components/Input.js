import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import addItem from '../store/actionCreators/addItem';

export default function Input() {

    const dispatch = useDispatch()
    const API_KEY = '84d4806cf446eaaca48f246a981554ac';
    const [inputValue, setInputValue] = useState('')

    function handleChange(e) {
        setInputValue(e.target.value)
    }

    function addCity(city) {
        dispatch(addItem(city));
        const cities = localStorage.getItem('cities');
        if (cities) {
            const updatedCities = JSON.parse(cities).filter(el => el.id !== city.id);
            updatedCities.push(city);
            localStorage.setItem('cities', JSON.stringify(updatedCities))
        } else {
            localStorage.setItem('cities', JSON.stringify([city]));
        }
    }

    async function handleSubmit(e) {
        try {
            e.preventDefault()
            const data = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${inputValue}&appid=${API_KEY}`);
            const city = await data.json()
            if(city.cod === '404') return;
            addCity(city)
        } catch(err) { }
    }

    return (
        <form onSubmit={handleSubmit} className="input-group mb-3">
            <input onChange={handleChange} value={inputValue} type="text" className="form-control" placeholder="Write your city" aria-label="Recipient's username" aria-describedby="basic-addon2" />
            <div className="input-group-append">
                <button className="btn btn-outline-secondary" type="submit">Button</button>
            </div>
        </form>
    )
}
