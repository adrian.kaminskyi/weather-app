import React from 'react'
import Card from '../components/Card'
import Input from '../components/Input'

export default function Home() {
    return (
        <div>
            <Input />
            <Card />
        </div>
    )
}
