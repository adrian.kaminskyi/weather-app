import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

export default function About(props) {
    
    const { cityId } = useParams();
    const [cityInfo, setCityInfo] = useState({});

    useEffect(() => {
      const cityArr = JSON.parse(localStorage.getItem('cities'));
      setCityInfo(cityArr.find(el => el.id.toString() === cityId));
    }, [cityId]);
    
    return (
        <div className="jumbotron jumbotron-fluid">
            <div className="container">
                <h2 className="display-6">{cityInfo.name}</h2>
                <h4 className="lead">Weather: {cityInfo.weather && cityInfo.weather[0].main.toLowerCase()}</h4>
                <h4 className="lead">Sky: {cityInfo.weather && cityInfo.weather[0].description}</h4>
                <h4 className="lead">Coordinates: lon: {cityInfo.coord && cityInfo.coord.lon}, lat: {cityInfo.coord && cityInfo.coord.lat}</h4>
                <h4 className="lead">Wind speed: {cityInfo.wind && cityInfo.wind.speed} km/h</h4>
                <h4 className="lead">Temperature: at day {cityInfo.main && Math.floor((cityInfo.main.temp_min - 32) * 5 / 9)} / at night {cityInfo.main && Math.floor((cityInfo.main.temp_max - 32) * 5 / 9)}</h4>

            </div>
        </div>
    )
}
