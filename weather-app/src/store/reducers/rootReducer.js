import { ADD_ITEM, REMOVE_ITEM, UPDATE_ITEM } from "../actions/actionTypes";

const initialState = {
    cities: []
}

export default function rootReducer (state = initialState, action) {
    switch(action.type) {
        case ADD_ITEM: {
            return {...state, cities: [...state.cities.filter(city => city.id !== action.payload.id), action.payload]};
        }
        case REMOVE_ITEM: {
            return {...state, cities: state.cities.filter(city => city.id !== action.payload.id)};
        }
        case UPDATE_ITEM: {
            return {...state, cities: [...state.cities.filter(city => city.id !== action.payload.id), action.payload]};
        }
        default: return state
    }
}
