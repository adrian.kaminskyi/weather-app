import { REMOVE_ITEM } from "../actions/actionTypes";

function removeItem (payload) {
    return {
        type: REMOVE_ITEM,
        payload
    };
}

export default removeItem