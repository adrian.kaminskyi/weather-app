import { UPDATE_ITEM } from "../actions/actionTypes";

function updateItem (payload) {
    return {
        type: UPDATE_ITEM,
        payload
    }
}

export default updateItem