import { ADD_ITEM } from "../actions/actionTypes";

function addItem (payload) {
    return {
        type: ADD_ITEM,
        payload
    };
}

export default addItem