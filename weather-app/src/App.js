import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import NavBar from './components/NavBar';
import Home from './pages/Home';
import About from "./pages/About";
function App() {
  return (
    <div className="container">
      <BrowserRouter >
        <NavBar />
        <Switch>
          <Route path={'/'} exact component={Home} />
          <Route path={'/about/:cityId'} exact component={About} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
